/*
**  Copyright (C) 2024  Anatoli Wagner <info@anwait.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


plugins {
    id("core-conventions")
    `kotlin-dsl`
}

version = libs.versions.java.conventions.get()

dependencies {
    implementation(libs.bundles.java.conventions)
}

gradlePlugin {
    plugins {
        matching {
            it.name == "org.anwait.java-conventions"
        }.configureEach {
            id = "org.anwait.java-conventions"
            displayName = "anwa[it] Java Conventions"
            description = "Common Java conventions"
            implementationClass = "Org_anwait_javaConventionsPlugin"
            tags = listOf("anwait", "java")
        }
    }
}

kotlin {
    jvmToolchain {
        languageVersion = JavaLanguageVersion.of(libs.versions.java.lang.get())
    }
}
