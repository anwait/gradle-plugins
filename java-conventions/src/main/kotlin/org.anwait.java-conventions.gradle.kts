/*
 * Copyright (C) 2024  Anatoli Wagner <git at anwait dot org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.gradle.internal.jvm.Jvm
import org.gradle.kotlin.dsl.invoke
import org.joda.time.LocalDate

plugins {
    java
    jacoco
    pmd
    id("org.owasp.dependencycheck")
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }

    withSourcesJar()
}

tasks.compileJava {
    options.compilerArgs.add("-Xlint:all")
}

tasks.jar {
    manifest {
        attributes(
            "Created-By" to Jvm.current().toString(),
            "Build-Date" to LocalDate.now().toString("yyyy-MM-dd"),
            "Implementation-Title" to project.name,
            "Implementation-Version" to project.version
        )
    }
}

// See https://dzone.com/articles/reproducible-builds-in-java
tasks.withType<AbstractArchiveTask> {
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
}

tasks.withType<Test> {
    useJUnitPlatform()

    failFast = true

    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = "0.5".toBigDecimal()
            }
        }
    }

    dependsOn(tasks.jacocoTestReport)
}

// Jacoco dependency
tasks.check {
    dependsOn(tasks.jacocoTestCoverageVerification)
}
