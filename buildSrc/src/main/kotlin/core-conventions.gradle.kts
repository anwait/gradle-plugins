/*
 * Copyright (C) 2024  Anatoli Wagner <git at anwait dot org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.gradle.accessors.dm.LibrariesForLibs

plugins {
    id("groovy-gradle-plugin")
    id("com.gradle.plugin-publish")
    id("com.github.ben-manes.versions")
}

group = "org.anwait.gradle"

repositories {
    gradlePluginPortal()
    mavenCentral()
}

val libs = the<LibrariesForLibs>()

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(libs.versions.java.lang.get())
    }
}



gradlePlugin {
    website = "https://gitlab.com/anwait/gradle-plugins"
    vcsUrl = "https://gitlab.com/anwait/gradle-plugins.git"
}
