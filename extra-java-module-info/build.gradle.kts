plugins {
    id("core-conventions")
}

version = libs.versions.java.module.info.get()

dependencies {
    implementation(libs.bundles.java.module.info)
}

gradlePlugin {
    plugins {
        // here we register our plugin with an ID
        register(project.name) {
            id = "org.anwait.${project.name}"
            displayName = "anwa[it] Extra Java Module Info"
            description = "Building Java Modules with Legacy Libraries"
            implementationClass = "org.anwait.gradle.java.extramoduleinfo.ExtraModuleInfoPlugin"
            tags = listOf("anwait", "java", "jigsaw", "jpms", "legacy")
        }
    }
}
