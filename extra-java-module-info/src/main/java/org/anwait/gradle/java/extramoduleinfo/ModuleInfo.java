package org.anwait.gradle.java.extramoduleinfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data class to hold the information that should be added as module-info.class to an existing Jar file.
 */
public class ModuleInfo implements Serializable {
  private final String moduleName;
  private final String moduleVersion;
  private final List<String> exports;
  private final List<String> requires;
  private final List<String> requiresTransitive;
  private final List<String> usedServices;
  private final Map<String, List<String>> providedServices;

  ModuleInfo(String moduleName, String moduleVersion) {
    this.moduleName         = moduleName;
    this.moduleVersion      = moduleVersion;
    this.exports            = new ArrayList<>();
    this.requires           = new ArrayList<>();
    this.requiresTransitive = new ArrayList<>();
    this.usedServices       = new ArrayList<>();
    this.providedServices   = new HashMap<>();
  }

  public void exports(String exports) {
    this.exports.add(exports);
  }

  public void requires(String requires) {
    this.requires.add(requires);
  }

  public void requiresTransitive(String requiresTransitive) {
    this.requiresTransitive.add(requiresTransitive);
  }

  public void uses(final String usedService) {
    usedServices.add(usedService);
  }

  public void providesWith(final String service, final String provider) {
    if (!providedServices.containsKey(service)) {
      providedServices.put(service, new ArrayList<>());
    }

    providedServices.get(service).add(provider);
  }

  public String getModuleName() {
    return moduleName;
  }

  protected String getModuleVersion() {
    return moduleVersion;
  }

  protected List<String> getExports() {
    return exports;
  }

  protected List<String> getRequires() {
    return requires;
  }

  protected List<String> getRequiresTransitive() {
    return requiresTransitive;
  }
}
