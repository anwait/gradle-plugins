# anwa[it] Gradle Plugins
## Table of Contents
- [How To](#how-to)
- [Java Conventions](#java-conventions)


## How To
### Check Dependencies
Use the [gradle versions plugin](https://github.com/ben-manes/gradle-versions-plugin):
```sh
gradle dependencyUpdates
```


## Java Conventions
A simple plugin to add the common Java conventions used by anwa[it].
